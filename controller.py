from json import dumps, loads

from redis import Redis
from requests import get
from starlette.applications import Starlette
from starlette.config import Config
from starlette.exceptions import HTTPException
from starlette.requests import Request
from starlette.responses import JSONResponse

config = Config('.env')
REDIS_PORT = config('REDIS_PORT', cast=int, default=6379)
REDIS_DB = config('REDIS_DB', default=0)
REDIS_HOST = config('REDIS_HOST', default='127.0.0.1')

redis_client = Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
api_controller = Starlette()

@api_controller.route('/incidents', methods=['GET'])
async def get_incidents(request: Request):
    response = redis_client.get('incidents:current')

    if response:
        response = loads(response)
    else:
        http_response = get(url='https://cdstm.runstat.us/api.json')
        response = http_response.json()
        redis_client.set('incidents:current', dumps(response))
        redis_client.expire('incidents:current', 600)

        if http_response.status_code != 200:
            raise HTTPException(status_code=500)

    return JSONResponse(response)


@api_controller.route('/contributions', methods=['GET'])
async def get_contributions(request: Request):
    response = redis_client.get('contributions:current')

    if response:
        response = loads(response)
    else:
        http_response = get(url='https://framagit.org/users/HiTechRabbit/calendar.json')
        response = http_response.json()
        redis_client.set('contributions:current', dumps(response))
        redis_client.expire('contributions:current', 600)

        if http_response.status_code != 200:
            raise HTTPException(status_code=500)

    return JSONResponse(response)