import uvicorn
from starlette.config import Config

from controller import api_controller

if __name__ == '__main__':
    config = Config('.env')
    SERVER_PORT = config('SERVER_PORT', cast=int, default=8088)
    SERVER_HOST = config('SERVER_HOST', default='127.0.0.1')
    api_controller.debug = config('DEBUG', cast=bool, default=False)

    uvicorn.run(api_controller, host=SERVER_HOST, port=SERVER_PORT)
