# Configuration NGINX

Pour que mes différents sites web puissent taper dans cette api, il faut que NGINX définissent des headers autorisant le CORS. 
À cela s'ajoute également des règles particulières qui ont à avoir avec les preflights (donc des requêtes s'exécutant avant la mienne et qui s'assurent que tout est en ordre niveau CORS)

Voici à quoi doit ressembler la configuration de NGINX:

```
add_header  'Access-Control-Allow-Origin' *;

# Preflighted requests
if ($request_method = OPTIONS ) {
    add_header "Access-Control-Allow-Origin"  *;
    add_header "Access-Control-Allow-Methods" "GET, POST, OPTIONS, HEAD";
    add_header "Access-Control-Allow-Headers" "Authorization, Origin, X-Requested-With, Content-Type, Accept";
    return 200;
}
```

Ces paramètres doivent se trouver dans un ```location * {}```