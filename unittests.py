import unittest

from starlette.testclient import TestClient

from controller import api_controller


class ApiTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.http_client = TestClient(api_controller)

    def test_areIncidentsAccessible(self):
        response = self.http_client.get('/incidents')

        data = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertTrue(data['url'])

    def test_areContributionsAccessible(self):
        response = self.http_client.get('/contributions')

        data = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertTrue(data)


if __name__ == '__main__':
    unittest.main()